# CI/CD with Microsoft VSTS
Lab 02: Creating your first CI Pipeline with VSTS Builds

---

# Tasks

 - Import a project repository
 
 - Create a CI pipeline

---

## Import a project repository

 - Browse to Repos:

<img alt="Image 1.1" src="images/task-1.1.png"  width="75%" height="75%">

 - Select "Import repository":

<img alt="Image 1.2" src="images/task-1.2.png"  width="75%" height="75%">


 - Import the repository below:
 
```
https://oauth2:u9XWV6qNpU7z9PzWRHF5@gitlab.com/build-release-vsts/dotnetcore-demo-app.git
```

<img alt="Image 1.3" src="images/task-1.3.png"  width="75%" height="75%">

---
 
## Create a Build Definition

 - Browse to Repos:

<img alt="Image 2.1" src="images/task-2.1.png"  width="75%" height="75%">


 - Create a new build pipeline

<img alt="Image 2.2" src="images/task-2.2.png"  width="75%" height="75%">
  
 - Select the build sources

```
Source: Azure Repos Git
Team Project: ci-cd-workshop
Repository: dotnetcore-demo-app
```

 - Select the "Empty Job" template

 ![Image 3](images/lab02-3.png)


 - Configure the pipeline to run in the Sela pool:

```
Agent Pool: Sela
```

<img alt="Image 2.4" src="images/task-2.4.png"  width="75%" height="75%">
 
 
 - Add a .NET Core step:

```
Display Name: Restore
Command: restore
Path to project(s): **/*.csproj
```

 ![Image 4](images/lab02-4.png)
 
 - Add a .NET Core step:

```
Display Name: Build
Command: build
Path to project(s): **/*.csproj
Arguments: --configuration $(BuildConfiguration)
```

 ![Image 5](images/lab02-5.png)
 
 - Add a .NET Core step:

```
Display Name: Test
Command: test
Path to project(s): **/*[Tt]ests/*.csproj
Arguments: --configuration $(BuildConfiguration)
```

 ![Image 6](images/lab02-6.png)

 - Add a .NET Core step:

```
Display Name: Publish
Command: publish
Path to project(s): **/*.csproj
Arguments: --configuration $(BuildConfiguration) --output $(build.artifactstagingdirectory)
```

 ![Image 7](images/lab02-7.png)
 
 - Add a Publish Build Artifacts step:

```
Display Name: Publish Artifact
Path to publish: $(build.artifactstagingdirectory)
Artifact name: drop
Artifact publish location: Visual Studio Team Services/TFS
```

 ![Image 8](images/lab02-8.png)

 
 - Update the BuildNumber format:

```
Build number format: $(BuildDefinitionName)-$(Rev:.r)
```

<img alt="Image 2.9" src="images/task-2.9.png"  width="75%" height="75%">
 
 
 - Configure the build to run nightly at 22:00:

```
Build number format: $(BuildDefinitionName)-$(Rev:r)
```

<img alt="Image 2.10" src="images/task-2.10.png"  width="75%" height="75%">
 
 
 - Save and queue the build
 